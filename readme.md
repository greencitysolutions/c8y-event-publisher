# Cumulocity Bash Events

Create a cumulocity event with a bash script.

Useage: `./create <type> <text> <device_id> <user> <password> <author>`. All arguments are optional but positional, meaning you have to pass all previous arguments to pass the next argument. All args have a default value:

| arg           | Default Value                     | Comment |
| ------------- | --------------------------------- | |
| `<type>`      | `gcs_irrigation_event`            | |
| `<text>`      | `Irrigation has been run`         | |
| `<device_id>` | Read from `~/credentials/id`      | |
| `<user>`      | Read from `~/credentials/user`    | For authorization |
| `<password>`  | Read from `~/credentials/password`| For authorization |
| `<author>`    | `<user>`                          | Additional field for event |